// XKill.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "XKill.h"

#include <sstream>
#include <cassert>

#define MAX_LOADSTRING 100

// Global Variables:
HWND appWnd;
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
TCHAR pointerWindowClass[MAX_LOADSTRING];
POINT pt;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
void HandleTick();
void PlaceWindow();
void StartLoop();

int APIENTRY _tWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPTSTR    lpCmdLine,
	int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_XKILL, szWindowClass, MAX_LOADSTRING);
	LoadString(hInstance, IDC_POINTER_WND, pointerWindowClass, MAX_LOADSTRING);

	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, 1))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_XKILL));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//��
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_XKILL));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_XKILL);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindowEx(WS_EX_LAYERED | WS_EX_TRANSPARENT, szWindowClass, szTitle, WS_POPUP, 0, 0, 13, 13, NULL, NULL, hInstance, NULL);

	SetLayeredWindowAttributes(hWnd,RGB(0,0,0),255,LWA_COLORKEY|LWA_ALPHA);

	if (!hWnd)
	{
		return FALSE;
	}

	appWnd = hWnd;

	ShowCursor(false);

	SetMenu(hWnd, NULL);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	BringWindowToTop(hWnd);

	SetTimer(appWnd, 1, 1, (TIMERPROC) NULL);
	SetTimer(appWnd, 2, 10, (TIMERPROC) NULL);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc, DCKillBmp;

	PAINTSTRUCT Ps;
	HBITMAP bmpKill;

	switch (message)
	{
	case WM_TIMER:
		if (wParam == 1){
			PlaceWindow();
		} else if (wParam == 2){
			HandleTick();
		}
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);

		// Load the bitmap from the resource
		bmpKill = LoadBitmap(hInst, MAKEINTRESOURCE(IDI_KILLICON));
		// Create a memory device compatible with the above DC variable
		DCKillBmp = CreateCompatibleDC(hdc);
		// Select the new bitmap
		SelectObject(DCKillBmp, bmpKill);

		// Copy the bits from the memory DC into the current dc
		BitBlt(hdc, 0, 0, 450, 400, DCKillBmp, 0, 0, SRCCOPY);

		// Restore the old bitmap
		DeleteDC(DCKillBmp);
		DeleteObject(bmpKill);

		EndPaint(hWnd, &ps);

		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void PlaceWindow(){
	GetCursorPos(&pt);

	int cursorW = GetSystemMetrics(SM_CXCURSOR);
	int cursorH = GetSystemMetrics(SM_CYCURSOR);

	int xToSet;
	int yToSet;

	xToSet = pt.x + 12;
	yToSet = pt.y + 20;

	if (&pt){
		SetWindowPos(appWnd,NULL,xToSet,yToSet,0,0,SWP_NOSIZE|SWP_NOZORDER);
	}

	return;
}

void HandleTick(){
	if(GetAsyncKeyState(VK_ESCAPE)) {
		ExitProcess(0);
		return;
	}

	if(GetAsyncKeyState(VK_LBUTTON)) {
		HWND  hwndPt;

		if(!&pt) {
			wprintf(L"GetCursorPos failed with %d\n", GetLastError());
			return;
		}

		int cursorX = GetSystemMetrics(SM_CXSCREEN);
		int cursorY = GetSystemMetrics(SM_CYSCREEN);

		if((hwndPt = WindowFromPoint(pt)) != NULL) {
			DWORD  dwTID;
			DWORD  dwPID;
			HANDLE hProcess;

			dwTID = GetWindowThreadProcessId(hwndPt, &dwPID);

			hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPID);

			if(hProcess == NULL) {
				wprintf(L"OpenProcess failed with error: %d\n", GetLastError());
			} else {
				TerminateProcess(hProcess, 1);

				CloseHandle(hProcess);
			}

			/*std::stringstream stream;
			stream << dwPID;
			std::string strPid = stream.str();

			std::string reqPref = "\ntaskkill /F /T /PID ";
			std::string req = reqPref + strPid;

			std::wstring wstrReq;
			for(int i = 0; i < req.length(); ++i) {
				wstrReq += wchar_t( req[i] );
			}

			const wchar_t* wReq = wstrReq.c_str();

			bool isSuccess = _tsystem(wReq);*/

			ExitProcess(0);
		}
	}

	return;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
