Windows equivalent of linux xkill utility.

Usage:
Run "xkill" and click window you want to close forcefully. 
Press ESC for cancel. 
Better to use in combination with AutoHotkey to start it up by shortcut. For example add this line to your AutoHotkey script file:
^+Delete::Run xkill

and xkill will run by ctrl + shift + del shortcut.